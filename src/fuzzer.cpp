#include <iostream> 
#include <cstdlib>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <string.h>
#include <cstring>
#include <iomanip>

using namespace std;

//author Yves kerbens Declerus && Bigodo Romeo 
string abcdfimagename; // @param: abcdfimagename used to store the user-input file name
static int crashcount=0;

/*In receiving a file this function will compute the file size
it takes @param: abdcfimagename
*/

int filelength() {  
   ifstream is;
   is.open(abcdfimagename);
   int count=0; char ch;
   while(!is.eof()){
     is.get(ch);
     count++;
   }
   return count;
}

/*
  The fileArray  method is utility method which takes  the bytes from a files and save them in array or characters
  @param filename ---> is the name of the file whose content is to be copied
  @param filesize ---> is the size of the file.
*/

string fileToArray(string filename, int filesize){
    char filearray[filesize];
    ifstream fileseeder(filename);
    if(fileseeder.good()){
        while(!fileseeder.eof()) {
            fileseeder.read(filearray, filesize);  //copying the byte from the  files to the buffer array
     }
    }
    return filearray;
}

/*
*/
const string generateDate(){
    time_t now = time(0);
    struct tm timestruct;
    char tbuffer[80];
    timestruct = *localtime(&now);
    strftime(tbuffer, sizeof(tbuffer), "%Y-%m-%d.%X", &timestruct);
    return tbuffer;
}

/*
  The saveBadInput function allows the program to save mutated files that have crashed the converter programm
  This algorith simply copy the last files that has crashed the program from fuzzedfiles folder to the logs directory
 */
void saveBadInput(){
    srand(time(NULL));
    int randnum = rand()%1000; //random number to be padded at the of saved bad files
    string logs = fileToArray("logs/converter_logs.txt", 100); //array of messages output from  the converter programm
    string cmdbuilderpart1 = "cp fuzzedfiles/fuzzedimage.img logs/crashingimageslogs"; 
    string cmdbuilderpart2 = generateDate(); //genetate system date to be appended to logs folder's file
    string cmdbuilderpart3 = ".img";
    string cmdbuilder = cmdbuilderpart1+cmdbuilderpart2+to_string(randnum)+cmdbuilderpart3; //Command to copy files from fuzzedfiles folder to logs folder
    char cmd[100];
    
    for (int i=0; i<=cmdbuilder.length(); i++){
      cmd[i]=cmdbuilder[i];
    }

    if (logs[0] == '*' && logs[1] == '*' && logs[2] == '*'){//check whether the program has been crashed by reading the first 3 char of
                                                            //logs/converter_logs.txt
	system(cmd);
	cout<<"Crach count #: "<<crashcount++<<endl<<endl;
      }
}

/*
This function  takes as @param mutationPercent and compute the @param: fuzzingfactor
*/

double  fuzzingFactorC(double mutationPercent) {
  return  mutationPercent * (filelength()-1);
}


void summaryDisplay(int completedtest, int ffactor, int numofcrach) {
  cout<<endl<<endl;
  cout<<"////////////////////////Summary/////////////////////////////////////"<<endl;
  cout<<"//Total Completed Test"<<"|"<<"# Of Changed Byte Per Test"<<"|"<<"Total # Of Crash//"<<endl;
  cout<<"//--------------------"<<"|"<<"--------------------------"<<"|"<<"----------------//"<<endl;
  cout<<"//"<<setw(10)<<completedtest-1<<setw(11)<<"|"<<setw(12)<<ffactor<<setw(15)<<"|"<<setw(8)<<numofcrach<<setw(10)<<"//"<<endl;
  cout<<"////////////////////////////////////////////////////////////////////"<<endl;
  cout<<"Binome: "<<"Yves Kerbens Declerus "<<" && "<<"Bigodo Romeo"<<endl;
}

/*
abcdMFuzzer()  randomly generates  characters and randomply picks up which place array indice should be replaces
it takes the user orginally provided files, changes some bytes according to the @param: fuzzingfactor
@param: numberoftest is the number of times the test will be repeated.
*/

void abcdMFuzzer( int numberoftest, double ffactor){
  int count = filelength(); //stores the user provided files length
  cout<<endl<<endl<<"Original image data size: " <<count-1<<endl;
  const int MAX = count; 
  char buffer[MAX];   //stores the the byte arrays files stored in @param: abdcfimagename
  string buffstr;

  int numberoftestcount;
  int random_position; //randomly generated position in the buffer array to  be changed
  int randbgenerator;  //randomly generated new  byte to change the buffer array values

  int fuzzingfactor = fuzzingFactorC(ffactor);
  
  ifstream fuzzingSeeder(abcdfimagename, ios::binary);
  
  buffstr = fileToArray(abcdfimagename, MAX);
  for(int i=0; i<=buffstr.length(); i++) {
      buffer[i] = buffstr[i];
  }

  cout<<"The percentage of data mutated: %" <<fuzzingfactor<<endl;
  
  for(numberoftestcount=1; numberoftestcount <=numberoftest; numberoftestcount++) {
    srand(time(NULL));
    cout<<endl<<"Test number: " <<numberoftestcount <<endl;
    for(int ffcount=1; ffcount<=fuzzingfactor;  ffcount++){
      random_position = rand()%(MAX-6)+5;
      randbgenerator = ((rand()%157) + 100);
      buffer[random_position] = (char) (randbgenerator);
      cout<<"fuzzing iteration " <<ffcount<<" ---> Byte position "<<random_position<<" values has been changed to: "<<randbgenerator<<endl;
    }  
  
    ofstream fuzzedimages("fuzzedfiles/fuzzedimage.img", ios::binary); //creating the modified images files and stores it in the fuzzedfiles directory
    fuzzedimages.write(buffer, MAX-1);
    fuzzedimages.close();
    system("./converter fuzzedfiles/fuzzedimage.img  convertedimages/fuzzedimageout.img 2>&1 | tee logs/converter_logs.txt"); //starting the converter tool and start the convertion
    saveBadInput(); //Calling the saveBadInput method to save files that have crashed
  }

  summaryDisplay(numberoftestcount, fuzzingfactor, crashcount);//Calling the display method to display the test report

}

//Checking the validity of mutation percentage(the second parameter passed from the command line)
bool testNumberValidityCheck(char* argtn) {
  string s (argtn);
  for (int i=0; i<s.length(); i++){
    if(s[i]== '0' || s[i]== '1' ||s[i]== '2' || s[i]== '3'|| s[i]== '5' ||s[i]== '6' || s[i]== '7'|| s[i]== '8' ||s[i]== '9'){
      cout<<"check is it ok" <<endl;
    } else {
      return false;
    }
  }
  return true;
}

//Checking the validity of mutation percentage(the third parameter passed from the command line)
bool fuzzFactorValidityCheck(char* argff) {
  string s(argff);
  for (int i=0; i<s.length(); i++){
    if(s[i]== '0' || s[i]== '1' ||s[i]== '2' || s[i]== '3'|| s[i]== '5' ||s[i]== '6' || s[i]== '7'|| s[i]== '8' ||s[i]== '9'|| s[i]== '.'){
    } else {
      return false;
    }
  }
  return true;
}		


int main(int argc, char * argv[]){
  
  string imagename (argv[1]); 
  int numbOfTest=0; //stores the number of tests  argument to be done (second argument)
  double fuzzingPerc=0.0; //stores mutation  percentage  argument to be done (third argument)
  int inl = imagename.length(); //compute the sizee of the first parameter passed in command line by users
 
  if (testNumberValidityCheck(argv[2])) {
    numbOfTest = atoi(argv[2]);
  } else{
    cout<<"Invalid input!!! each input should be a number from 0 to 9!"<<endl;
  }

  if (fuzzFactorValidityCheck(argv[3])) {
    fuzzingPerc = atof(argv[3]);
  } else{
    cout<<"Invalid input!!! Your input should have a '.' && digits 0-9!"<<endl;
  }
 
  if (((imagename[inl-4] == '.')&&(imagename[inl-3] == 'i') && (imagename[inl-2]== 'm') && (imagename[inl-1]== 'g')) &&
      (numbOfTest > 0) &&  ((fuzzingPerc > 0.0) && (fuzzingPerc<=1.0) )) {
      abcdfimagename =(argv[1]);
      abcdMFuzzer(numbOfTest, fuzzingPerc);
      
  } else {
    if ((imagename[inl-4] != '.')&&(imagename[inl-3] != 'i') && (imagename[inl-2] != 'm') && (imagename[inl-1] != 'g')){
      cout<<"This is not a valid abcd format picture, Please give a valid abcd file"<<endl;
    }

    if (numbOfTest<= 0){
      cout<<"Invalid input, your second argument should be a number higher than 0 "<<endl;
    }

    if (fuzzingPerc <= 0.0 || fuzzingPerc>1.0 ){
      cout<<"Invalid input, your third argument should be a decimal number"
	  <<endl<<"higher than 0 and lower or equal to 1"<<endl;
    }

  }
 

}
    
