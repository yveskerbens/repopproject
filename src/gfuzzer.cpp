#include <iostream> 
#include <cstdlib>
#include <string>
#include <fstream>
#include <ctime>

using namespace std;

char buffer[288]; //define an array of 288 byte to save the value of  abcd formate image to be generated

//The databuilder method generates a valid abcd formate image.
//each Bytes is primarily saved in an array
//@Author: Yves Kerbens Declerus
void dataBuilder() {
    buffer[0] = (char) 171;
    buffer[1] = (char) 205;
    buffer[2] = (char) 100;
    buffer[3] = (char) 0;
    buffer[4] = (char) 16;
    buffer[5] = (char) 0;
    buffer[6] = (char) 0;
    buffer[7] = (char) 0;
    buffer[8] = (char) 16;
    buffer[9] = (char) 0;
    buffer[10] = (char) 0;
    buffer[11] = (char) 0;
    buffer[12] = (char) 4;
    buffer[13] = (char) 0;
    buffer[14] = (char) 0;
    buffer[15] = (char) 0;
    buffer[16] = (char) 0;
    buffer[17] = (char) 0;
    buffer[18] = (char) 0;
    buffer[19] = (char) 255;
    buffer[20] = (char) 255;
    buffer[21] = (char) 0;
    buffer[22] = (char) 255;
    buffer[23] = (char) 0;
    buffer[24] = (char) 255;
    buffer[25] = (char) 0;
    buffer[26] = (char) 0;
    buffer[27] = (char) 0;
    buffer[28] = (char) 0;
    buffer[29] = (char) 255;
    buffer[30] = (char) 0;
    buffer[31] = (char) 0;
  
    
    for(int i=1; i<=17; i++){
      for(int j=0; j<=15; j++){
	if( j == 0 || j == 7 || j == 8 || j == 15) {
	  buffer[16*(i+1)+j] = (char) 0;
	}
	if( j == 1 || j == 6 || j == 9 || j == 14){
	  buffer[16*(i+1)+j] = (char) 01;
	}
	if( j == 2 || j == 5 || j == 10 || j == 13){
	  buffer[16*(i+1)+j] = (char) 2;
	}
	  if( j == 3 || j == 4 || j == 11 || j == 12){
	    buffer[16*(i+1)+j] = (char) 3;
	  }
	     
         }
      }
    cout<<endl;
}

//copy the buffer's value to genetedimage.img files in the notnomodifiedfiles folder
void makeFile(char* buff){
    ofstream imageFile("notmodifiedfiles/generatedimage.img", ios::binary);
    imageFile.write(buff, 288);
}

//This method randomly picks up a these case
//These case are based on byte value that can crash the program except the 4th case.
void abcdFuzzer(){
    srand(time(NULL));
    int randCase = (rand()%(4)) + 1;
    switch(randCase) {
        case 1:
            buffer[11] = (char) 232;
	    cout<<"The program was crashed due to case 1: wrong width "<<endl;
            break;

        case 2:
	   buffer[14] = (char) 224;
           buffer[15] = (char) 183;
	   cout<<"The program was crashed due to change in byte 14 && 16 (wrong height) "<<endl;
           break;
        
        case 3:
	    buffer[15] = (char) 251;
	    cout<<"The program was crashed due to wrong input on byte 16 (height) "<<endl;
            break;

        case 4:
           buffer[5] = (char) 241;
	   cout<<"The conversion should pass if you reach this stage, as this test case has no value to crash the program!!!"<<endl;
           break;
    
    }

    ofstream fuzzedimages("fuzzedfiles/gfuzzedimage.img", ios::binary);
    fuzzedimages.write(buffer, 288);
    fuzzedimages.close();
    system("./converter fuzzedfiles/gfuzzedimage.img  convertedimages/gfuzzedimageout.img");
     
}
int main(int argc, char *argv[]){
    dataBuilder();
    makeFile(buffer);
    abcdFuzzer();
    for(int i=0; i<=287; i++){cout << buffer[i] << " -- ";}
    cout<<endl;
}



