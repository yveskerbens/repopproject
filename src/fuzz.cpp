#include <iostream> 
#include <cstdlib>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>


using namespace std;

char* abcdfimagename;
int fuzzingfactor;

int filelength() {  
   ifstream is;
   is.open(abcdfimagename);
   int count=0; char ch;
   while(!is.eof()){
     is.get(ch);
     count++;
   }
   return count;
}


double  fuzzingFactorC(double fuzzingfactor) {
  return  fuzzingfactor * (filelength()-1);
}

 
void abcdMFuzzer( int numberoftest){
  int count = filelength();
  cout<<endl<<endl<<"Original image data size: " <<count-1<<endl;
  const int MAX = count; 
  char buffer[MAX];
  int random_position;
  int randbgenerator;	
  int numberoftestcount =1;
  

  ifstream fuzzingSeeder(abcdfimagename, ios::binary);
  
  
  if(fuzzingSeeder.good()){
     while(!fuzzingSeeder.eof()) {
       fuzzingSeeder.read(buffer, MAX);
    }
  }
  
  cout<<"The percentage of data mutated: %" <<fuzzingfactor<<endl;
  
  for(numberoftestcount; numberoftestcount <=numberoftest; numberoftest++ ) {
    srand(time(NULL));
    cout<<endl<<"Test number: " <<numberoftestcount <<endl;
    for(int ffcount=1; ffcount<=fuzzingfactor;  ffcount++){
      random_position = rand()%(MAX-6)+5;
      randbgenerator = (rand()%157) + 100;
      buffer[random_position] = (char) (randbgenerator);
      cout<<"fuzzing iteration " <<ffcount<<" ---> Byte position "<<random_position<<" values has been changed to: "<<randbgenerator<<endl;
    }  
  
    
  
    ofstream fuzzedimages("fuzzedfiles/fuzzedimage.img", ios::binary);
    fuzzedimages.write(buffer, MAX-1);
    fuzzedimages.close();
    system("./converter fuzzedfiles/fuzzedimage.img  fuzzedimageout.img");
    numberoftestcount++;
    }

}

int main(int argc, char * argv[]){
  abcdfimagename =(argv[1]); 
  fuzzingfactor = fuzzingFactorC(atof(argv[3]));
  abcdMFuzzer(atoi(argv[2]));
}
    

