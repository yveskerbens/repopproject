### Computer Systems Security (Fuzzer Project)


## Fuzzer compilation and run 
   Go inside the folder “Fuzzer” and run for:


#  1. Mutation-based fuzzer

> the command line: 
> ./runfuzzer notmodifiedfiles/testinput.img 1000 0.1 <br/>

    where
     - ‘notmodifiedfiles/testinput.img’ is the path which contains the picture with indexed color  ‘testinput.img’. 
	The .img’ extension is required, otherwise the fuzzer wont recognize the picture. 

     - ‘1000’  is the number of test

     -  ‘0.1’ is a percentage


#   2. Generation-based fuzzer

	> the command line:  
	> ./rungfuzzer  <br/>


About folders inside the .Zip document:


   -  Notmodifiedfiles contains the right  generated picture with indexed color 
      which will be modified in order to crash or not the tool.

   -  Fuzzedfiles contains modified version of the picture which could potentially crash or not the tool. 
      Only the last generated picture is saved because the previous one have been erase by the following one 
      in order to keep a storage memory in the computer. 
      For example, when we do 10000 test we does not want to save or keep all the 10000 pictures generated. 
      Each time one of those generated pictures crash the tool, it is saved in Logs.

   -  Logs contains pictures which crashed the tool by differents way.
   -  Bad_input_files contains 3 bad input files
   -  Src contains the source code


## What the bad input files contain


Folder “bad_input_files” contains 3 bad input files: 


   - ‘gfuzzedimage_1.img’ which contains a wrong byte number 11 ‘e8’

   - ‘gfuzzedimage_2.img’ which contains a wrong byte number 15 ‘fb’

   - ‘gfuzzedimage_3.img’ which contains two wrong bytes number 14, 15’ ->  ‘e0’ , ‘fb’